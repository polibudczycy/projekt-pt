var express = require('express');
var router = express.Router();

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var secrets = require('./../../secrets');
var url = 'mongodb://localhost:27017/projekt-pt';
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

function runQuery(query) {
  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    query(db, function() {
        db.close();
    });
  });
}

function sortObject(obj) {
    var arr = [];
    var prop;
    for (prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop]
            });
        }
    }
    arr.sort(function(a, b) {
        return a.value - b.value;
    });
    return arr; // returns array
}

function recommendFriends(screenName, renderCallback) {
  var sum = {};

  function find(db, callback1) {
    db.collection('users').findOne({
      "screen_name": screenName
    }, function(err, document) {

      if (!document) {
        console.log('!document');
        exec('node ../index.js ' + screenName + ' &');

        ls = spawn('node', ['../index.js', screenName]);

        ls.stdout.on('data', function (data) {
          console.log('stdout: ' + data);
        });

        ls.stderr.on('data', function (data) {
          console.log('stderr: ' + data);
        });

        ls.on('exit', function (code) {
          console.log('child process exited with code ' + code);
        });
        renderCallback({ not_prepared: true });
        callback1();
        return;
      }

      if (document.preparing_recommendations) {
        renderCallback({ not_prepared: true });
        console.log('document.preparing_recommendations');
        callback1();
        return;
      }

      function getFollowing(db, callback) {

        var cursor = db.collection('users').find({
          "_id": {
            "$in": document.friends_ids,
          }
        });

        cursor.each(
          function(err, doc) {
            assert.equal(err, null);

            if (doc !== null) {

              if (doc.friends_ids) {
                for (var i = 0; i < doc.friends_ids.length; i++) {
                  var friendOfFriendId = doc.friends_ids[i];
                  if (document.friends_ids.indexOf(friendOfFriendId) == -1 && friendOfFriendId != document.id) {
                    if (!sum[friendOfFriendId]) {
                      sum[friendOfFriendId] = 0;
                    }
                    sum[friendOfFriendId] += 1;
                  }
                }
              }

            } else {
              var ids_recommendations = sortObject(sum);
              var ids = ids_recommendations.map(function(id) {
                return parseInt(id.key, 10);
              }).reverse();

              ids = ids.slice(0, 100);

              db.collection('users').find({
                "_id": {
                  "$in": ids,
                }
              }).toArray(function(err, items) {
                assert.equal(null, err);

                for (var id in ids_recommendations) {
                  if (ids_recommendations.hasOwnProperty(id)) {
                    var current_id = ids_recommendations[id];

                    for (var i = 0; i < items.length; i++) {
                      if (items[i]._id == current_id.key) {
                        items[i].recommendation_score = current_id.value;
                        break;
                      }
                    }

                  }
                }

                items.sort(function (a,b) {
                  if (a.recommendation_score > b.recommendation_score)
                    return -1;
                  else if (a.recommendation_score < b.recommendation_score)
                    return 1;
                  else
                    return 0;
                });

                renderCallback({ items: items });
                callback();
              });
            }
          }
        );
      }

      runQuery(getFollowing);

      callback1();
    });
  }

  runQuery(find);
}

function getGraphData(screenName, renderCallback) {
  var sum = {};

  function find(db, callback1) {
    db.collection('users').findOne({
      "screen_name": screenName
    }, function(err, document) {

      var data = {
        nodes: [],
        edges: [],
      };

      data.nodes.push({
        id: document._id,
        label: document.screen_name,
      });
      // data.nodes.push({data: { id: document._id }});

      function getFollowing(db, callback) {

        var cursor = db.collection('users').find({
          "_id": {
            "$in": document.friends_ids,
          }
        });

        cursor.each(
          function(err, doc) {
            assert.equal(err, null);

            if (doc !== null) {

              if (doc.friends_ids) {
                for (var i = 0; i < doc.friends_ids.length; i++) {
                  var friendOfFriendId = doc.friends_ids[i];
                  if (document.friends_ids.indexOf(friendOfFriendId) == -1 && friendOfFriendId != document.id) {
                    if (!sum[friendOfFriendId]) {
                      sum[friendOfFriendId] = 0;
                    }

                    data.edges.push({
                      from: doc._id,
                      to: friendOfFriendId,
                    });
                    // data.edges.push({data: { source: doc._id, target: friendOfFriendId }});
                    sum[friendOfFriendId] += 1;
                  }
                }
              }

            } else {
              var ids_recommendations = sortObject(sum);
              var ids = ids_recommendations.map(function(id) {
                return parseInt(id.key, 10);
              }).reverse();

              ids = ids.slice(0, 100);

              var length = data.edges.length;
              for (var i = 0; i < length; i++) {
                if (!data.edges[i]) {
                  break;
                }
                var index = ids.indexOf(data.edges[i].to);
                // var index = ids.indexOf(data.edges[i].data.target);
                if (index == -1) {
                  data.edges.splice(i, 1);
                  i--;
                }
              }

              db.collection('users').find({
                "_id": {
                  "$in": ids,
                }
              }).toArray(function(err, items) {
                assert.equal(null, err);

                for (var i = 0; i < items.length; i++) {
                  data.nodes.push({
                    id: items[i]._id,
                    label: items[i].screen_name,
                  });
                  // data.nodes.push({data: { id: items[i]._id }});
                }

                db.collection('users').find({
                  "_id": {
                    "$in": document.friends_ids,
                  }
                }).toArray(function(err, friends) {
                  assert.equal(null, err);

                  for (var i = 0; i < friends.length; i++) {
                    data.nodes.push({
                      id: friends[i]._id,
                      label: friends[i].screen_name,
                    });
                    // data.nodes.push({data: { id: friends[i]._id }});

                    data.edges.push({
                      from: document._id,
                      to: friends[i]._id,
                    });
                    // data.edges.push({data: { source: document._id, target: friends[i]._id }});
                  }

                  renderCallback(data);
                  callback();
                });

              });
            }
          }
        );
      }

      runQuery(getFollowing);

      callback1();
    });
  }

  runQuery(find);
}

router.get('/login',
  function(req, res, next) {
    res.redirect('/login/twitter');
  }
);

router.get('/app',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res, next) {
    recommendFriends(req.user.username, function(data) {
      data.loggedUser = req.user;
      // console.log(data);
      res.render('app', data);
    });
  }
);

router.get('/graph',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res, next) {
    var data = {};
    data.loggedUser = req.user;
    res.render('graph', data);
  }
);

router.get('/graph/nodes',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res, next) {
    getGraphData(req.user.username, function(data) {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(data));
    });
  }
);

router.get('/',
  function(req, res, next) {
    var data = {};
    data.loggedUser = req.user;
    res.render('index', data);
  }
);

module.exports = router;
