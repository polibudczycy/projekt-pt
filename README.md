# README #

In order to run the application you need to do the following:

1. Register a new app on [apps.twitter.com](https://apps.twitter.com/)
2. Copy the `secrets.example.js` to `secrets.js`.
3. Fill in the keys (they are in the 'Keys and Access Tokens' tab). You might need to generate Access Token at the bottom of the page.
4. Run `node index.js`.