var Twit = require('twit');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var secrets = require('./secrets');
var Bottleneck = require("bottleneck");
var url = 'mongodb://localhost:27017/projekt-pt';

// twitter api helper lib
var T = new Twit(Object.create(secrets, {
  timeout_ms: {
    value: 60*1000,
  },
}));

// queue that lets us play nice with the api rate limit
// one request at the time, every 61 seconds
var limiter = new Bottleneck(1, 61*1000);

function runQuery(query) {
  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    query(db, function() {
        db.close();
    });
  });
}

/* this runs the whole process
1. gets your details from the api
2. adds `friends_ids` field to your entry in the db
3. after that, it saves profiles of all the people that you follow to the db as well
*/
function saveUserToDb(screenName) {
  T.get('users/lookup', { screen_name: screenName },  function (err, data, response) {

    data[0]._id = data[0].id;
    data[0].preparing_recommendations = true;

    var insertUser = function(db, callback) {
       db.collection('users').insertOne(data[0], function(err, result) {
        assert.equal(err, null);

        callback();
        saveFollowingIdsToDb(screenName, true);
      });
    };

    runQuery(insertUser);
  });
}

// retrieves following ids for given user
function saveFollowingIdsToDb(screenName, shouldSaveUsers, cb) {
  T.get('friends/ids', { screen_name: screenName },  function (err, data, response) {

    console.log('saveFollowingIdsToDb', '---START----');
    console.log('saveFollowingIdsToDb', screenName);
    console.log('saveFollowingIdsToDb', '---KONIEC---');

    var update = function(db, callback) {
       db.collection('users').updateOne(
          {
            "screen_name" : screenName
          },
          {
            $set: {
              "friends_ids": data.ids
            },
          }, function(err, results) {

          if (shouldSaveUsers) {
            saveUsersById(screenName);
          }

          callback();

          if (cb) {
            cb();
          }
       });
    };

    runQuery(update);
  });
}

function chunkArray(array, chunkSize) {
  var arrayParts = [];

  for (var i = 0; i < array.length; i += chunkSize) {
    arrayParts.push(array.slice(i, i + chunkSize));
  }

  return arrayParts;
}

// saves user profiles of people that given user follows
function saveUsersById(screenName) {

  function insertUsers(users) {

    var save = function(db, callback) {
       db.collection('users').insertMany(users, function(err, result) {
        // assert.equal(err, null);
        callback();
      });
    };

    runQuery(save);
  }

  function findFollowingIds(db, callback) {
    var cursor = db.collection('users').find({
      "screen_name": screenName,
    });
    cursor.each(
      function(err, doc) {
        assert.equal(err, null);

        if (doc !== null) {
          var followingChunks = chunkArray(doc.friends_ids, 100);

          followingChunks.forEach(function(el) {
            T.get('users/lookup', { user_id: el },  function (err, data, response) {
              data.map(function(user) {
                user._id = user.id;

                return user;
              });
              insertUsers(data);
            });
          });

        } else {
            hydrateFriendsFollowingField(screenName);
           callback();
        }
      }
    );
  }

  runQuery(findFollowingIds);
}

var screen_name = process.argv[2];
console.log(screen_name);
saveUserToDb(screen_name);
// recommendFriends(screen_name);

function saveUsersByIds(users_ids) {

  function insertUsers(users) {

    users.forEach(function(user) {
      var save = function(db, callback) {
         db.collection('users').insert(user, function(err, result) {
          // assert.equal(err, null);
          callback();
        });
      };

      runQuery(save);
    });
  }

  var followingChunks = chunkArray(users_ids, 100);

  followingChunks.forEach(function(el) {
    T.get('users/lookup', { user_id: el },  function (err, data, response) {
     data.map(function(user) {
       user._id = user.id;

       return user;
     });
     insertUsers(data);
    });
  });
}

/*
  argument: your user name
  reads all the people you follow from the database, and adds `friends_ids` field to their entities.
  due to twitter api limitations this is super slow - 1 user per minute (number of users that you follow = how long it's gonna take in minutes).
*/
function hydrateFriendsFollowingField(screenName) {
  function find(db, callback) {
    db.collection('users').findOne({
      "screen_name": screenName
    }, function(err, document) {

      function getFollowing(db, callback) {

        var cursor = db.collection('users').find({
          "_id": {
            "$in": document.friends_ids,
          }
        });

        cursor.each(
          function(err, doc) {
            assert.equal(err, null);

            if (doc !== null) {

              if (!doc.friends_ids) {
                console.log(doc.screen_name);
                limiter.submit(saveFollowingIdsToDb, doc.screen_name, false, null);
              }

            } else {

              var update = function(db, callback2) {
                 db.collection('users').updateOne(
                    {
                      "screen_name" : screenName
                    },
                    {
                      $set: {
                        "preparing_recommendations": false
                      },
                    }, function(err, results) {

                    callback2();
                 });
              };

              runQuery(update);

              callback();
            }
          }
        );
      }

      runQuery(getFollowing);

      callback();
    });
  }

  runQuery(find);
}

function sortObject(obj) {
    var arr = [];
    var prop;
    for (prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop]
            });
        }
    }
    arr.sort(function(a, b) {
        return a.value - b.value;
    });
    return arr; // returns array
}



function recommendFriends(screenName) {
  var sum = {};

  function find(db, callback) {
    db.collection('users').findOne({
      "screen_name": screenName
    }, function(err, document) {

      function getFollowing(db, callback) {

        var cursor = db.collection('users').find({
          "_id": {
            "$in": document.friends_ids,
          }
        });

        cursor.each(
          function(err, doc) {
            assert.equal(err, null);

            if (doc !== null) {

              if (doc.friends_ids) {
                for (var i = 0; i < doc.friends_ids.length; i++) {
                  var friendOfFriendId = doc.friends_ids[i];
                  if (document.friends_ids.indexOf(friendOfFriendId) == -1 && friendOfFriendId != document.id) {
                    if (!sum[friendOfFriendId]) {
                      sum[friendOfFriendId] = 0;
                    }
                    sum[friendOfFriendId] += 1;
                  }
                }
              }

            } else {
              var ids = sortObject(sum);
              ids = ids.slice(ids.length-101, ids.length-1).map(function(id) {
                return id.key;
              });

              console.log(ids);
              saveUsersByIds(ids);
              callback();
            }
          }
        );
      }

      runQuery(getFollowing);

      callback();
    });
  }

  runQuery(find);
}

// recommendFriends('pimpl');